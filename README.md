Localization Update Bundled
===========================

The Localization Update Bundled module exposes bundled project translations
so you can easily track changes and update when needed.


## Requirements

* Interface Translation module


## Installation

Enable the module as normal, your bundled translations should be available
after refreshing the translations information.


## Notes

The po files must have the same name as in the languages table, e.g. `nl.po`
and they must be stored in the `translations` folder of your theme/module.

For example: `my_module/translations/nl.po`
