Changelog
=========

All notable changes to the Localization Update Bundled module.


## 2.2.0 (2024-09-18):

### Added

* Drupal 11 compatibility.


## 2.1.0 (2023-05-10):

### Added

* Drupal 10 compatibility.


## 2.0.0 (2020-03-24):

### Added

* Drupal 9 compatibility.

### Removed

* Header based modification time detection (core uses the file modification time).


## 8.x-1.0 (2018-09-06):

Initial release.
